
# awk -f script.awk < models.pdb
# awk -f ./split.awk -v pdbname=2n97 < ./unittest/2n97.pdb

BEGIN { f=1; fname=pdbname"_"f".pdb"}
/ENDMDL/ {
  print "ENDMDL" > fname
  getline; 
  f++; 
  fname=pdbname"_"f".pdb"; 
}
{ if($0~/^ATOM/ || $0 ~/^TER/) print $0 > fname }
